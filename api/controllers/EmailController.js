/**
 * EmailController
 *
 * @description :: Server-side logic for managing Emails
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var queueManager = require('../manager/QueueManager.js');

// var mysql      = require('mysql');
// var pool = mysql.createPool({
//   connectionLimit : 10,
//   host     : 'localhost',
//   user     : 'root',
//   password : 'ok',
//   database : 'gridle_master'
// });


module.exports = {
    
    taskcompleted: function(req, res) {
        global.pool.query('SELECT email_verified from `users` WHERE id =\'' + data.id + '\'', function(err, rows, fields) {
            if (err) {
                throw err;
            }
            console.log('sql query fired');
            console.log(rows);

        });
        queueManager.getemailinstance();
        return res.json({
            todo: 'done execution'
        });
    },

    signup: function(req, res) {
        try {
            var data = JSON.stringify(req.body);
            var data1 = JSON.parse(data);
            console.log(data1);
            console.log('hit here on new emails');
            queueManager.getQueueWorker('math', 'sendInvitationEmail', data1);
            // queueManager.getDelayedQueueWorker(20000, 'math', 'sendInvitationEmail', data1);
            // queueManager.getDelayedQueueWorker(345600000, 'math', 'sendVerificationReminderEmail', data1);
            // queueManager.getDelayedQueueWorker(604800000, 'math', 'sendVerificationLinkExpiration', data1);
            // queueManager.getDelayedQueueWorker(2592000000, 'math', 'sendUpgradation', data1);
            // queueManager.getDelayedQueueWorker(3888000000, 'math', 'sendUpgradationReminder', data1);            
            // queueManager.getDelayedQueueWorker(172800000, 'math', 'sendWelcome', data1);
            // queueManager.getDelayedQueueWorker(864000000, 'math', 'sendBestFeature', data1);
            // queueManager.getDelayedQueueWorker(1296000000, 'math', 'sendAboutTasks', data1);
            // queueManager.getDelayedQueueWorker(1728000000, 'math', 'sendAboutProject', data1);
            // queueManager.getDelayedQueueWorker(3456000000, 'math', 'sendFeedback', data1);

            return res.json({
                status: 200,
                message: 'Email successfully pushed to queue'
            });
        } catch (err) {
            console.log(err);
        }
    },

    taskStatusUpdate: function(req, res) {
        var data = JSON.stringify(req.body);
        var data1 = JSON.parse(data);       
        queueManager.getQueueWorker('math', 'sendTaskStatusUpdate', data1);
        return res.json({
            status: 200,
            message: 'Email successfully pushed to queue'
        });
    },

    taskdeleted: function(req, res) {
        global.pool.query('SELECT email_verified from `users` WHERE id =\'' + data.id + '\'', function(err, rows, fields) {
            if (err) {
                throw err;
            }
            console.log('sql query fired');
            console.log(rows);

        });
        queueManager.getemailinstance();
        return res.json({
            todo: 'done execution'
        });
    },

    taskAdd: function(req, res) {
        var data = JSON.stringify(req.body);
        var data1 = JSON.parse(data);
        console.log('taskadd queue');
        queueManager.getQueueWorker('math', 'sendTaskCreated', data1);
        return res.json({
            status: 200,
            message: 'Email successfully pushed to queue'
        });
    },


    taskDelete: function(req, res) {
        var data = JSON.stringify(req.body);
        var data1 = JSON.parse(data);
        console.log(data1);
        queueManager.getQueueWorker('math', 'sendTaskDeleted', data1);
        return res.json({
            status: 200,
            message: 'Email successfully pushed to queue'
        });
    },




    taskfileadd: function(req, res) {

        var data = JSON.stringify(req.body);
        var data1 = JSON.parse(data);
        console.log(data1);


        return res.json({
            todo: 'done execution'
        });
    },

    taskfileremove: function(req, res) {
        var data = JSON.stringify(req.body);
        var data1 = JSON.parse(data);
        console.log(data1);

        return res.json({
            todo: 'done execution'
        });
    },

    invite: function(req, res) {
        var data = JSON.stringify(req.body);
        var data1 = JSON.parse(data);
        //create email settings logic here ,
        //
        queueManager.getQueueWorker('math', 'sendInvitationMail', data1);
        queueManager.getDelayedQueueWorker(345600000, 'delayed', 'sendInvitationReminderEmail', data1);
    }
};
