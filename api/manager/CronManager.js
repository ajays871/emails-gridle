var Queue = require('../manager/QueueManager.js');
var cron = require('node-schedule');
var emailservice = require('../services/EmailService.js');
var mysql = require('mysql');
var lodash = require('lodash');
var dateFormat = require('dateformat');
var NR = require("node-resque");
var CronJob = require('cron').CronJob;

var pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: 'ok',
    database: 'gridle_master'
});

var sendDailyTaskList = function() {

    var now = new Date();
    var todayDate = dateFormat(now, 'isoDate');
    console.log(todayDate, 'today date');
    pool.query('SELECT id, first_name, last_name, email from `users` WHERE active = 1', function(err, rows, fields) {
        if (err) {
            throw err;
        }

        var stringdata = JSON.stringify(rows);
        var isverified = JSON.parse(stringdata);

        console.log(rows.length > 0);
        if (rows.length > 0) {
            console.log('round');
            lodash.each(rows, function(r) {
                console.log(r);
                var userId = r.id;

                pool.query('Select task_id from `task_assignments` where isDeleted=false AND user_id =\'' + userId + '\'', function(err1, rows1, fields1) {

                    // var taskList = lodash.pluck(rows1, 'task_id');
                    var taskList = _.pluck(rows1, 'task_id').map(function(d) {
                        return d ? d : null;
                    });
                    
                    var query = pool.query('SELECT * FROM tasks WHERE isDeleted = false and DATE(end_date) = \'' + todayDate + '\' and id in (' + mysql.escape(taskList) + ')', function(err, results) {
                        console.log(results, 'results');
                        var email_payload = {};
                        var options = {
                            subject: 'Hey, Your today list of tasks on Gridle.',
                            template: 'task/dailyTaskList.ejs',
                            from: 'theteam@gridle.io',
                            to: r.email
                        };
                        email_payload.tasks = results;
                        email_payload.username = r.first_name + ' ' + r.last_name;                       
                        emailservice.construct();
                        emailservice.sendemailv1(email_payload, options);
                    });
                })
            })
        }
    });
};

var exports = module.exports = {};

exports.construct = function() {
    console.log('cron manager construct called');
    /*
            1. sending daily agenda mail
            2. sending weekly project report
            3. sending weekly analytics report
            4. generating invoice in pdf, sending invoice mails.
            5. deducting credits from workspaces on weekly basis
    

            */


    //daily task list scheduler

    // new CronJob('* * * * * *', function() {
    //     sendDailyTaskList();
    // }, null, true, 'America/Los_Angeles');

    //daily 
};
