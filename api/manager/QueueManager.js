var queue = require("node-resque");
var emailservice = require('../services/EmailService.js');
var mysql = require('mysql');
var lodash = require('lodash');
var dateFormat = require('dateformat');

var pool = mysql.createPool({
    connectionLimit: 10,
    host: '127.0.0.1',
    user: 'root',
    password: 'ok',
    database: 'gridle_master'
});

var schedule = require('node-schedule');


var exports = module.exports = {};

exports.getemailinstance = function() {

    this.defaultqueue.enqueueIn(5000, 'math', 'sendmail');
    //this.defaultqueue.enqueue('math', 'sendmail');
};

exports.getQueueWorker = function(queue, job, args) {

    this.defaultqueue.enqueue(queue, job, args);

};

exports.getDelayedQueueWorker = function(delay, queue, job, args) {

    this.defaultqueue.enqueueIn(delay, queue, job, args);
};

//construct function for app mailer.
//it setup jobs, start scheduler, Queue, and workers.
// contruct is called on bootstrap before lifting the sails server.


exports.construct = function() {
    var self = this;
    var jobs = {

        "sendTaskCreated": {
            perform: function(data, callback) {

                var created_by = lodash.find(data.object.Users, function(x) {
                    return parseInt(x.User.id) === parseInt(data.object.Task.Task.created_by);
                });

                created_by = created_by.User.name;
                lodash.each(data.object.Users, function(user) {
                    console.log('inside loop', user);
                    var options = {
                        subject: 'Task Created',
                        template: 'task/created.ejs',
                        from: 'theteam@gridle.io',
                        to: user.User.email
                    };
                    var project_name = "";
                    if ((data.object.Task.Task.project_id !== -1) && (data.object.Task.Task.project_id !== null)) {
                        project_name = data.object.Project.Project.title;
                    }
                    console.log(data.object.Task)
                    var contextData = {
                        title_html: data.object.Task.Task.title_html,
                        created_by: created_by,
                        username: user.User.name,
                        end_date: data.object.Task.Task.end_date,
                        project_name: project_name,
                        users: data.object.Users
                    };
                    console.log('context data', contextData);
                    emailservice.construct();
                    emailservice.sendemailv1(contextData, options);
                })
            }
        },

        "sendTaskStatusUpdate": {
            perform: function(data, callback) {

                /*
                Data object should have 
                data.object
                            Task
                            User
                            Users
                            Old_Task
                            Project

                */

                var changed_by = lodash.find(data.object.Users, function(y) {

                    return parseInt(y.User.id) === parseInt(data.object.Changed_by);
                })

                lodash.each(data.object.Users, function(user) {
                    console.log('inside loop', user);
                    var options = {
                        subject: 'Task Status Update',
                        template: 'task/statuschange.ejs',
                        from: 'theteam@gridle.io',
                        to: user.User.email
                    };
                    console.log(options, 'options');
                    var project_name = -1;
                    console.log(data.object.Task_new);
                    if ((data.object.Task_new.Task.project_id !== -1) && (data.object.Task_new.Task.project_id !== null)) {
                        project_name = data.object.Project.Project.title;
                    }


                    var contextData = {
                        title_html: data.object.Task_new.Task.title_html,
                        changed_by: changed_by.User.name,
                        old_status: data.object.Task_old.Task.status,
                        new_status: data.object.Task_new.Task.status,
                        username: user.User.name,
                        end_date: dateFormat(data.object.Task_new.Task.end_date),
                        project_name: project_name,
                        users: data.object.Users
                    };
                    console.log('context data', contextData);
                    emailservice.construct();
                    emailservice.sendemailv1(contextData, options);
                })



            }
        },

        "sendTaskDeleted": {
            perform: function(data, callback) {

                deleted_by = lodash.find(data.object.Users, function(x) {

                    return parseInt(x.User.id) === parseInt(data.object.Deleted_by);
                });

                console.log(data.object.Users, 'users');
                lodash.forEach(data.object.Users, function(user) {
                    var options = {
                        subject: 'Task Deleted',
                        template: 'task/deleted.ejs',
                        from: 'theteam@gridle.io',
                        to: user.User.email
                    };
                    console.log(data.object, deleted_by )
                    var project_name = -1;
                    
                    if ((data.object.Task_old.Task.project_id !== -1) && (data.object.Task_old.Task.project_id !== null)) {
                        project_name = data.object.Project.Project.title;
                    }
                    var contextData = {
                        title_html: data.object.Task_old.Task.title_html,
                        deleted_by: deleted_by.User.name,
                        username: user.User.name,
                        end_date: dateFormat(data.object.Task_old.Task.end_date),
                        project_name: project_name
                    };
                    console.log('context data', contextData);
                    emailservice.construct();
                    emailservice.sendemailv1(contextData, options);
                });
            }
        },

        "sendmail": {
            perform: function(callback) {
                pool.query('SELECT * FROM `users`', function(err, rows, fields) {
                    if (err) throw err;
                    emailservice.construct();
                    var emaildata = {
                        emailfrom: 'theteam@gridle.io',
                        emailto: 'ajay_sharma@gridle.io'
                    };
                    var emailoptions = {};
                    emailservice.sendemail(emaildata, emailoptions);
                });

            },
        },

        "sendVerificationEmail": {
            perform: function(data, callback) {

                var options = {
                    subject: 'Welcome to Gridle. One small thing',
                    template: 'welocome/welcome',
                    from: 'theteam@gridle.io',
                    to: 'ajay_sharma@gridle.io'
                };
                emailservice.construct();

                emailservice.sendemail(data, options);
            },
        },

        "sendInvitationEmail": {
            perform: function(data, callback) {
                var options = {
                    subject: 'Let’s use Gridle for working together. Here’s your invite link.',
                    template: 'welcome/invitation.ejs',
                    from: 'theteam@gridle.io',
                    to: data.object.User.email
                };
                emailservice.construct();

                emailservice.sendemail(data, options);
                return;
            },
        },

        "sendVerificationReminderEmail": {
            perform: function(data, callback) {

                var options = {
                    subject: 'Urgent. Verification required for your Gridle account',
                    template: 'welocome/verificationreminder',
                    from: 'theteam@gridle.io',
                    to: data.to
                };
                pool.query('SELECT email_verified from `users` WHERE id =\'' + data.id + '\'', function(err, rows, fields) {
                    if (err) {
                        throw err;
                    }


                    var stringdata = JSON.stringify(rows);
                    var isverified = JSON.parse(stringdata);

                    if (rows[0].email_verified === 0) {

                        emailservice.construct();
                        emailservice.sendemail(data, options);
                    }
                });
            },
        },

        "sendInvitationReminderEmail": {
            perform: function(data, callback) {

                var options = {
                    subject: 'Hey, I am waiting for you to join ' + data.workspace_name + ' on Gridle.',
                    template: 'welcome/invitationreminder',
                    from: 'theteam@gridle.io',
                    to: data.to
                };
                emailservice.construct();
                emailservice.sendemail(data, options);
            },
        },

        "sendVerificationLinkExpiration": {
            perform: function(data, callback) {

                var options = {
                    subject: 'Your verification link had expired. Here’s how to get a new one.',
                    template: 'welocome/verificationlinkexpire',
                    from: 'theteam@gridle.io',
                    to: data.to
                };
                emailservice.construct();
                emailservice.sendemail(data, options);
            },
        },

        "sendInvitationLinkExpiration": {
            perform: function(data, callback) {

                var options = {
                    subject: 'Welcome to Gridle. One small thing',
                    template: 'welocome',
                    from: 'theteam@gridle.io',
                    to: data.to
                };
                emailservice.construct();
                emailservice.sendemail(data, options);
            },
        },

        "sendUpgradationReminder": {
            perform: function(data, callback) {

                var options = {
                    subject: 'About upgrading Gridle and some exciting features you’ll get back!',
                    template: 'welocome/upgradereminder',
                    from: 'theteam@gridle.io',
                    to: data.to
                };
                emailservice.construct();
                emailservice.sendemail(data, options);
            },
        },

        "sendUpgradation": {
            perform: function(data, callback) {

                var options = {
                    subject: ' So, it’s our 1 month anniversary today!',
                    template: 'welocome/upgradereminder',
                    from: 'theteam@gridle.io',
                    to: data.to
                };
                emailservice.construct();
                emailservice.sendemail(data, options);
            },
        },

        "sendGuestInvitation": {
            perform: function(data, callback) {

                var options = {
                    subject: 'Let’s use Gridle for our work.',
                    template: 'welocome',
                    from: 'theteam@gridle.io',
                    to: data.to
                };
                emailservice.construct();
                emailservice.sendemail(data, options);
            },
        },

        "sendPaymentVerification": {
            perform: function(data, callback) {

                var options = {
                    subject: 'Let’s use Gridle for our work.',
                    template: 'paymentverification',
                    from: 'theteam@gridle.io',
                    to: data.user_info.user.email
                };
                emailservice.construct();
                emailservice.sendemail(data, options);
            },
        },

        "sendPaymentSuccessfull": {
            perform: function(data, callback) {


                var options = {
                    subject: 'Let’s use Gridle for our work.',
                    template: 'paymentsuccessfull',
                    from: 'theteam@gridle.io',
                    to: data.user_info.user.email
                };
                emailservice.construct();
                emailservice.sendemail(data, options);
            },
        },

        "startcron": {
            perform: function(data, callback) {


            },
        }
    };

    var connectionDetails = {
        package: 'ioredis',
        host: '127.0.0.1',
        password: null,
        port: 6379,
        database: 0,
    };
    //  var connectionDetails = {
    //     package: 'ioredis',
    //     host: ' redis-test-gridle.zue6bu.0001.usw2.cache.amazonaws.com',
    //     password: null,
    //     port: 6379,
    //     database: 0,
    // };


    var scheduler = new queue.scheduler({
        connection: connectionDetails
    });

    this.scheduler = scheduler;

    scheduler.connect(function() {
        scheduler.start();
    });

    var multiWorkerInstant = new queue.multiWorker({
        connection: connectionDetails,
        queues: ['math'],
    }, jobs);

    var multiworkerDelayed = new queue.multiWorker({
        connection: connectionDetails,
        queues: ['delayedQueue']
    }, jobs);



    var defaultqueue = new queue.queue({
        connection: connectionDetails,
    }, jobs);

    var delayedQueue = new queue.queue({
        connection: connectionDetails,
    }, jobs);

    defaultqueue.on('error', function(error) {

    });

    defaultqueue.connect(function() {

    });

    this.defaultqueue = defaultqueue;

    delayedQueue.on('error', function(error) {

    });

    delayedQueue.connect(function() {

    });

    this.delayedQueue = delayedQueue;

    var scheduler = new queue.scheduler({
        connection: connectionDetails
    });

    scheduler.connect(function() {
        scheduler.start();
    });

    multiWorkerInstant.on('start', function() {

    });

    multiWorkerInstant.on('success', function(queue, job, result) {

    });

    multiworkerDelayed.on('start', function() {

    });

    multiworkerDelayed.on('success', function(queue, job, result) {

    });

    scheduler.on('start', function() {

    });
    scheduler.on('end', function() {

    });
    scheduler.on('poll', function() {

    });
    scheduler.on('master', function(state) {

    });
    scheduler.on('error', function(error) {

    });
    scheduler.on('working_timestamp', function(timestamp) {

    });
    scheduler.on('transferred_job', function(timestamp, job) {

    });
    defaultqueue.on('start', function() {

    });

    delayedQueue.on('start', function() {

    });

    multiWorkerInstant.start();
    multiworkerDelayed.start();
};

exports.scheduler = function() {
    return this.scheduler;
};
