var exports = module.exports = {};

var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');

var path = require('path');
global.appRoot = path.resolve(__dirname);

var templatePath = '/var/www/emails-gridle/templates/';

// lib/moduleA/component1.js

// var hbs = require('nodemailer-express-handlebars');
// var handlebars = require('handlebars');

/*
Require filesystem node library to read template files
Require EJS lib to build html from the template
*/

var fs = require('fs');
var ejs = require('ejs')
ejs.open = '{{';
ejs.close = '}}';

exports.sendemail = function(data, options) {
    var templateFile = fs.readFileSync('/var/www/emails-gridle/templates/' + options.template, 'utf-8');

    var htmlFile = ejs.render(templateFile, {
        username: data.object.User.first_name,
        verification_token: data.object.User.verification_token,
        filename: '/var/www/emails-gridle/templates/' + options.template
    });
    var emailOptions = {
        from: options.from,
        to: data.object.User.email,
        subject: options.subject,
        html: htmlFile,
        attachments: [{
            filename: 'empty-email.png',
            contentType: 'image/png',
            cid: 'empty-email',
            path: '/var/www/emails-gridle/templates/attachments/empty-email.png'
        }, {
            filename: 'facebook.png',
            contentType: 'image/png',
            cid: 'facebook',
            path: '/var/www/emails-gridle/templates/attachments/facebook.png'
        }, {
            filename: 'gridle-logo-washed.png',
            contentType: 'image/png',
            cid: 'gridle-logo-washed',
            path: '/var/www/emails-gridle/templates/attachments/gridle-logo-washed.png'
        }, {
            filename: 'gridle-logo.png',
            contentType: 'image/png',
            cid: 'gridle-logo',
            path: '/var/www/emails-gridle/templates/attachments/gridle-logo.png'
        }, {
            filename: 'linkedin.png',
            contentType: 'image/png',
            cid: 'linkedin',
            path: '/var/www/emails-gridle/templates/attachments/linkedin.png'
        }, {
            filename: 'twitter.png',
            contentType: 'image/png',
            cid: 'twitter',
            path: '/var/www/emails-gridle/templates/attachments/twitter.png'
        }]
    };

    this.transporter.sendMail(emailOptions, function(error, info) {
        if (error) {
            // write retry logic here to send email
            // integrate papertrial/errorception for error alerts

            console.log('error while sending email', error, info);
            return;
        }
        console.log('message sent : ', info.response);
        return;
    });
};

exports.sendemailv1 = function(data, options) {
    
    var templateFile = fs.readFileSync('/var/www/emails-gridle/templates/' + options.template, 'utf-8');
    console.log('datacontext', data);
    var emailContextData = data;
    emailContextData.filename = '/var/www/emails-gridle/templates/' + options.template;
   // console.log(emailContextData.users[1], 'email context data');

    var htmlFile = ejs.render(templateFile,
        emailContextData
    );
    console.log('html', htmlFile);
    var emailOptions = {
        from: options.from,
        to: options.to,
        subject: options.subject,
        html: htmlFile       
    };

    this.transporter.sendMail(emailOptions, function(error, info) {
        if (error) {
            // write retry logic here to send email
            // integrate papertrial/errorception for error alerts

            console.log('error while sending email', error, info);
            return;
        }
        console.log('message sent : ', info.response);
        return;
    });
};

exports.construct = function() {
    var options = {
        port: 465,
        host: 'email-smtp.us-west-2.amazonaws.com',
        secure: true,
        auth: {
            user: 'AKIAIDDP42IY743Q4RPQ',
            pass: 'AstD/F7dLnMeR9VmMJjddMMnNyVND8Ga2Y0qsvN1gNR4'
        }
    };
    var templateoptions = {
        viewEngine: {
            extname: '.hbs',
            layoutDir: 'templates/'
        },
        viewPath: 'templates/',
        extName: '.ejs'
    };
    var transporter = nodemailer.createTransport(smtpTransport(options));
    this.transporter = transporter;
};
